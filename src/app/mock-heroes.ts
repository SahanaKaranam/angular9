import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Yash' },
      { id: 2, name: 'Mahesh Babu' },
      { id: 3, name: 'Prabhas' },
      { id: 4, name: 'Ram' },
      { id: 5, name: 'Chiranjeevi' },
      { id: 6, name: 'Nagarjuna' },
      { id: 7, name: 'Puneeth' },
      { id: 8, name: 'Venkatesh' },
      { id: 9, name: 'Hrithik' },
      { id: 10, name: 'Uday Kiran' }
];
