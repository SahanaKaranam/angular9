import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Yash' },
      { id: 2, name: 'Mahesh Babu' },
      { id: 3, name: 'Prabhas' },
      { id: 4, name: 'Ram' },
      { id: 5, name: 'Chiranjeevi' },
      { id: 6, name: 'Nagarjuna' },
      { id: 7, name: 'Puneeth' },
      { id: 8, name: 'Venkatesh' },
      { id: 9, name: 'Hrithik' },
      { id: 10, name: 'Uday Kiran' }
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 1;
  }
}
